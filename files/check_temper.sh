#!/bin/bash
#
#check_temp.sh: Script checks the temperature of QSFP+ transceivers
#
# Copyright (C) 2017 CSIRT, Institute of Computer Science, Masaryk University
#
# Author(s):    Marian REHAK    <rehak@ics.muni.cz>
#
# Attention:
# -nrpe user must be allowed to execute this specific script(because of ethtool) as root
# -add the following llines to visudo
#  # Allows running a script as root with fixed arguments
#    nrpe    ALL=(root)  NOPASSWD: /usr/lib64/nagios/plugins/check_temper.sh -w 45 -c 50
# 
# -add this row below Default specifications
#  Defaults: nrpe !requiretty
#
# -in /etc/nagios/nrpe.cfg run this script as "sudo"
#
#-----------------------------------------------------------------------------
#                                  Script
#-----------------------------------------------------------------------------
#Assign treshold values
while getopts "w:c" option; do
        case "${option}" in
                w) WARNING=${OPTARG};;
                c) CRITICAL=${OPTARG};;
        esac
done

# ethtool execute variable
if which ethtool 1>/dev/null 2>&1; then

        ETHTOOL=`which ethtool`
else

        echo "Error: ethtool is not installed!"

        exit 2

fi

#Get temperature in 2 digits format
TEMPER1=$(ethtool -m eth2 | grep temperature | head -1 | sed 's/.*: \([0-9]*\).*/\1/')
TEMPER2=$(ethtool -m eth3 | grep temperature | head -1 | sed 's/.*: \([0-9]*\).*/\1/')

if [ $TEMPER1 -gt $TEMPER2 ]; then

        TEMPER=$TEMPER1

else

        TEMPER=$TEMPER2
fi

#Exit - assign exit state STATE_OK=0, STATE WARNING=1, STATE_CRITICAL=2
if [ $TEMPER -lt $WARNING ]; then

        EXIT_STATE=0
fi
if [ $TEMPER -gt $WARNING ]; then
        if [ $TEMPER -lt $CRITICAL ]; then
                EXIT_STATE=1
        else
                EXIT_STATE=2
        fi
fi

echo "Temperature is: $TEMPER"
#Exit state, which Nagios will use to display temperature status.
exit $EXIT_STATE

